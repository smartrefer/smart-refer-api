import Knex = require('knex');
import { HisMbaseModel } from './his_mbase.model';
const hospcode = process.env.HIS_CODE;

export class HisHimproHiModel {

  async getLogin(db: Knex, username: any, password: any) {
    // let data = await db.raw(`
    //    SELECT LOWER(userlogin) as 'username',username as 'fullname',pcucode as 'hcode' FROM hosdata.user WHERE userlogin = '${username}' and RIGHT(cid,4)='${password}' and pcucode != ''`);
    // return data[0];
    let data = await db(`hosdata.user`)
    .select(`user.userlogin as username`, `user.username as fullname`)
    .select(db(`hosdata.confighos`).first(`confighos.codehos`).as(`hcode`).limit(1))
    .where(`user.userlogin`, username)
    .andWhereRaw(`SUBSTRING(user.cid, 10, 13) = ?`, [password]);
return data;
  }

  async getServices(db: Knex, hn: any, seq: any, referno: any) {
    let cmd = `SELECT CONCAT(r.regdate,'-',r.frequency,'-',r.seq)'seq',p.pttitle 'title_name',p.ptfname 'first_name',p.ptlname 'last_name',r.daterefer 'date_serv',r.timerefer 'time_serv',hos.getRoomName(r.depart)'department' FROM hosdata.sendrefer r INNER JOIN pt.pt p on r.hn = p.hn WHERE r.regdate = SeqSmartReferToRegdate('${seq}') and RIGHT(CONCAT('000000000',r.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and r.frequency = SeqSmartReferToFrequency('${seq}') and r.seq = SeqSmartReferToSeqSendRefer('${seq}')`;
    let data = await db.raw(cmd);
    // console.log("SQL Command Services : " + cmd);
    return data[0];
  }

  async getProfile(db: Knex, hn: any, seq: any, referno: any) {
    let cmd = `SELECT p.hn,REPLACE(cardid,'-','')'cid',pttitle 'title_name',ptfname 'first_name',ptlname 'last_name',if(ptsex='sx1','ชาย','หญิง')'sex',ptvillage 'moopart',ptaddress 'addrpart',pttambon 'tmbpart',ptamphur 'amppart',ptprovince 'chwpart',ptdob 'brthdate',concat(right(concat('000',timestampdiff(YEAR,ptdob,NOW())),3) ,'-',right(concat('00',timestampdiff(month,ptdob,NOW())-(timestampdiff(year,ptdob,NOW())*12)),2), '-',right(concat('00',timestampdiff(day,date_add(ptdob,interval (timestampdiff(month,ptdob,NOW())) month),curdate())),2)) 'age',c.ptclass 'pttype_id',(SELECT i.NAME FROM hos.insclasses i WHERE c.ptclass = i.code)'pttype_name',c.classNo 'pttype_no',c.mainhos 'hospmain',hos.getHospitalName(c.mainhos)'hospmain_name',c.subhos 'hospsub',hos.getHospitalName(c.subhos)'hospsub_name',p.regdate 'registdate',p.regdate 'visitdate',CONCAT(p.fattitle,p.fatfname,' ',p.fatlname)'father_name',CONCAT(p.mottitle,p.motfname,' ',p.motlname)'mother_name','' as 'couple_name',CONCAT(p.contacttitle,p.contactfname,' ',p.contactlname)'contact_name',(SELECT h.NAME FROM hos.codeinhos h WHERE h.code = p.relation)'contact_relation',p.ptphone 'contact_mobile' FROM pt.pt p LEFT JOIN pt.ptclass c on p.hn = c.hn WHERE RIGHT(CONCAT('000000000',p.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) GROUP BY p.hn `;
    let data = await db.raw(cmd);
    // console.log("getProfile : " + cmd);
    return data[0];
  }

  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT codehos 'provider_code',nameHos 'provider_name' FROM hosdata.confighos`);
    return data[0];
  }

  async getAllergyDetail(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`SELECT listname 'ndrug_name',listsign 'symptom',daterecord 'begin_date',daterecord  FROM pt.ptallergy WHERE RIGHT(CONCAT('000000000',hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and listname != 'ไม่มีประวัติแพ้'`); //Edit 2022-08-30
    return data[0];
  }

  async getChronic(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
        SELECT icd10 'icd_code',dateadm 'start_date',(SELECT i.description FROM hos.icd101_4 i WHERE i.code = c.icd10 GROUP BY i.code)'icd_name' FROM pt.ptclinic c WHERE RIGHT(CONCAT('000000000',hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) ORDER BY dateadm DESC`);

    return data[0];
  }

  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`SELECT '${seq}' as 'seq',s.daterefer 'date_serv','00:00' as 'time_serv',d.diag 'icd_code',d.descrip 'icd_name',d.dxtype 'diag_type','' as 'DiagNote',d.listdiag 'diagtype_id' FROM sendrefer s INNER JOIN opd.odiag d on s.regdate = d.regdate and s.hn = d.hn and s.frequency =  d.frequency WHERE s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}')

        UNION
        
        SELECT '${seq}' as 'seq',s.daterefer 'date_serv','00:00' as 'time_serv',d.diag 'icd_code',d.descrip 'icd_name',d.dxtype 'diag_type','' as 'DiagNote',d.listdiag 'diagtype_id' FROM sendrefer s INNER JOIN ipd.idiag d on s.an = d.an WHERE s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}')`);
    return data[0];
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
       SELECT '${seq}' as 'seq',an,(SELECT replace(p.cardid,'-','') FROM pt.pt p WHERE p.hn = s.hn)'pid',s.hn,REPLACE(refsendno,'/','-')'referno',daterefer 'referdate',sendtohos 'to_hcode',c.ptclass 'pttype_id',hos.getPtClassName(c.ptclass)'pttype_name',SmartReferGetStrength(s.refhigh)'strength_id',SmartReferGetStrengthName(s.refhigh)'strength_name',hos.getRoomName(s.depart)'location_name',if( s.an != '','IPD','OPD')'station_name',convertLoadNoToSmartRefer(s.refcomein)'loads_id',convertLoadToSmartRefer(s.refcomein)'loads_name',hos.getHospitalName(s.sendtohos)'to_hcode_name',(SELECT replace(d.cause,'\r\n','') FROM referdata d WHERE s.regdate = d.regdate and s.hn = d.hn and s.frequency = d.frequency and s.seq = d.seq)'refer_cause',timerefer 'refertime',getDoctorNo(s.doctor)'doctor',s.doctor as 'doctor_name',(SELECT d.hist FROM referdata d WHERE s.regdate = d.regdate and s.hn = d.hn and s.frequency = d.frequency and s.seq = d.seq)'refer_remark' FROM hosdata.sendrefer s LEFT JOIN pt.ptclass c on s.hn = c.hn WHERE s.regdate = SeqSmartReferToRegdate('${seq}') and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}') and seq = SeqSmartReferToSeqSendRefer('${seq}') GROUP BY s.hn`);

    return data[0];
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
  
    let cmd = `SELECT '${seq}' AS 'seq',regdate 'date_serv',time_order 'time_serv',
	REPLACE (namedrug,'"','') 'drug_name',amount 'qty',(
	SELECT i.UnitName FROM hos.itemlist i WHERE i.itemcode=d.codedrug) 'unit',item_usage 'usage_line1','' AS 'usage_line2','' AS 'usage_line3' FROM opd.drug_order_opd d WHERE regdate=LEFT ('${seq}',10) AND RIGHT(CONCAT('000000000',hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) AND frequency=split_str ('${seq}','-',4) UNION
	SELECT '${seq}' AS 'seq',orderdate 'date_serv',time_order 'time_serv',
	REPLACE (namedrug,'"','') 'drug_name',amount 'qty',(
	SELECT i.UnitName FROM hos.itemlist i WHERE i.itemcode=d.codedrug) 'unit',item_usage 'usage_line1','' AS 'usage_line2','' AS 'usage_line3' FROM ipd.drug_order_ipd d LEFT JOIN ipd.ipd ii ON ii.an=d.an WHERE ii.regdate=LEFT ('${seq}',10) AND RIGHT(CONCAT('000000000',ii.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) AND ii.frequency=split_str ('${seq}','-',4)`;
    let data = await db.raw(cmd); //Edit 2022-08-30
    // console.log("getDrugs : " + cmd);
    return data[0];
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let cmd = `SELECT
 s.regdate 'date_serv',
 s.timerefer 'time_serv',(
 SELECT
  i.NAME 
 FROM
  hos.lablist i 
 WHERE
  i.CODE = l.labcode 
 ) 'labgroup',
 l.labname 'lab_name',
 l.result_lab 'lab_result',
 '' AS 'unit',
 ifnull( l.normal_lab, '' ) 'standard_result' 
FROM
 hdata.sendrefer s
 INNER JOIN opd.result_lab_opd l ON s.regdate = l.regdate 
 AND s.hn = l.hn 
 AND l.frequency = s.frequency 
WHERE
 s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' 
 AND RIGHT(CONCAT('000000000',s.hn),9) = RIGHT ( CONCAT( '000000000', '${hn}' ), 9 ) 
 AND s.frequency = SeqSmartReferToFrequency ( '${seq}' ) 
 UNION
SELECT
 l.regdate 'date_serv',
 l.time_order 'time_serv',(
 SELECT
  i.NAME 
 FROM
  hos.lablist i 
 WHERE
  i.CODE = ls.labcode 
 ) 'labgroup',
 ls.labname 'lab_name',
 ls.result_lab 'lab_result',
 '' AS 'unit',
 ifnull( ls.normal_lab, '' ) 'standard_result' 
FROM
 opd.lab_order_opd l
 INNER JOIN opd.result_lab_opd ls ON l.regdate = ls.regdate 
 AND l.hn = ls.hn 
 AND l.frequency = ls.frequency 
WHERE
 l.regdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' 
 AND RIGHT(CONCAT('000000000',l.hn),9) = RIGHT ( CONCAT( '000000000', '${hn}' ), 9 ) 
 AND l.frequency = SeqSmartReferToFrequency ( '${seq}' ) 
 UNION
SELECT
 l.orderdate 'date_serv',
 l.time_order 'time_serv',(
 SELECT
  i.NAME 
 FROM
  hos.lablist i 
 WHERE
  i.CODE = ls.labcode 
 ) 'labgroup',
 ls.labname 'lab_name',
 ls.result_lab 'lab_result',
 '' AS 'unit',
 ifnull( ls.normal_lab, '' ) 'standard_result' 
FROM
 ipd.lab_order_ipd l
 INNER JOIN ipd.result_lab_ipd ls ON l.an = ls.an 
WHERE
 l.orderdate = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' 
 AND RIGHT(CONCAT('000000000',l.hn),9) = RIGHT ( CONCAT( '000000000', '${hn}' ), 9 )
  `;
    let data = await db.raw(cmd);
    // console.log("getLabs : " + cmd);
    return data[0];
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
    let data = await db.raw(`
  SELECT '${seq}' as 'seq',s.regdate 'vstdate',s.daterefer 'date_serv',p.appointdate 'date',p.timeappoint 'time',hos.getRoomName(p.toroomappoint)'department',p.causeappoint 'detail',s.timerefer 'time_serv' FROM sendrefer s INNER JOIN pt.ptappoint p on s.regdate = p.regdate and s.hn = p.hn WHERE s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServ)}' and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9)`);

    return data[0];
  }

  async getVaccine(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
        SELECT s.daterefer 'date_serv',s.timerefer 'time_serv',v.code 'vacine_code',(SELECT h.name FROM hos.vaccine h WHERE h.code = v.code)'vaccine_name' FROM sendrefer s INNER JOIN opd.ptvaccine v on s.regdate = v.regdate and s.hn = v.hn WHERE RIGHT(CONCAT('000000000',v.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9)`);

    return data[0];
  }

  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {

    let cmd = `SELECT s.hn 'pid','${seq}' as 'seq',s.regdate 'date_serv',o.timein 'time_serv',o.oper 'procedure_code',o.descrip 'procedure_name',o.regdate 'start_date',o.timein 'start_time',o.regdate 'end_date',o.timeout 'end_time' FROM sendrefer s INNER JOIN opd.ooper o on s.regdate = o.regdate and s.hn = o.hn and s.frequency = o.frequency WHERE RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' and s.frequency = SeqSmartReferToFrequency('${seq}') and seq = SeqSmartReferToSeqSendRefer('${seq}') 

        UNION
        
        SELECT s.hn 'pid','${seq}' as 'seq',s.regdate 'date_serv',o.start_time 'time_serv',o.oper 'procedure_code',o.descrip 'procedure_name',o.startdate 'start_date',o.start_time,o.enddate 'end_date',o.end_time FROM sendrefer s INNER JOIN ipd.ioper o on s.an = o.an WHERE RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' and s.frequency = SeqSmartReferToFrequency('${seq}') and seq = SeqSmartReferToSeqSendRefer('${seq}')`;
    let data = await db.raw(cmd);
    // console.log("getProcedure : " + cmd);
    return data[0];
  }

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let cmd = `SELECT '${seq}' as 'seq',s.regdate 'date_serv',o.timereg 'time_serv',(SELECT p.blood_group FROM pcu.person p WHERE s.hn = p.hn limit 1)'bloodgrp',o.weight,o.high 'height',o.bmi,o.temper 'temperature',o.pulse 'pr',o.respiration 'rr',o.hpressure 'sbp',o.lpressure 'dbp',o.sign 'symptom',o.scrroom 'depcode',hos.getRoomName(o.scrroom) 'department','' as 'movement_score','' as 'vocal_score','' as 'eye_score','' as 'oxygen_sat','' as 'gak_coma_sco',o.dxtext 'diag_text','' as 'pupil_right','' as 'pupil_left' FROM sendrefer s INNER JOIN opd.opd o on s.regdate = o.regdate and s.hn = o.hn and s.frequency = o.frequency WHERE s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}') and s.seq = SeqSmartReferToSeqSendRefer('${seq}')`;
    let data = await db.raw(cmd);
    // console.log("getNurture : " + cmd);
    return data[0];
  }

  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let cmd = `SELECT '${seq}' as 'seq',i.pi FROM sendrefer s INNER JOIN opd.opd_pi i  on s.regdate = i.regdate and s.hn = i.hn and s.frequency = i.frequency WHERE s.daterefer = SeqSmartReferToRegdate('${seq}') and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}') and s.seq = SeqSmartReferToSeqSendRefer('${seq}')`;
    let data = await db.raw(cmd);
    console.log("getPhysical : " + cmd);
    return data[0];
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let cmd = `SELECT '${seq}' as 'seq',replace(i.pi,'\r\n','') as 'hpi' FROM sendrefer s INNER JOIN opd.opd_pi i  on s.regdate = i.regdate and s.hn = i.hn and s.frequency = i.frequency WHERE s.daterefer = SeqSmartReferToRegdate('${seq}') and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}') and s.seq = SeqSmartReferToSeqSendRefer('${seq}')  group by s.regdate,s.hn,s.frequency,s.seq limit 1`;
    let data = await db.raw(cmd);
    console.log("getPillness : " + cmd);
    if(data.length > 0){
 return data[0];
}else{
 return null;
}
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let cmd = `SELECT s.regdate 'xray_date',x.namexray 'xray_name' FROM sendrefer s INNER JOIN opd.xray_order_opd x on s.regdate = x.regdate and s.hn = x.hn and s.frequency = x.frequency WHERE s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}') and s.seq = SeqSmartReferToSeqSendRefer('${seq}')

        UNION
        
        SELECT x.orderdate 'xray_date',x.namexray 'xray_name' FROM sendrefer s left JOIN ipd.ipd i on s.an = i.an INNER JOIN ipd.xray_order_ipd x on i.an = x.an WHERE s.daterefer = '${HisHimproHiModel.DateToYYYYMMDD(dateServe)}' and RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and s.frequency = SeqSmartReferToFrequency('${seq}') and s.seq = SeqSmartReferToSeqSendRefer('${seq}')`;
    let data = await db.raw(cmd);
    console.log("getXray : " + cmd);
    return data[0];
  }

  async getBedsharing(db: Knex) {
    let data = await db.raw(`
        SELECT now_ward 'ward_code',hos.getRoomName(now_ward)'ward_name',count(*)'ward_pt',ifnull((SELECT n.bed FROM hos.roomno n WHERE n.roomcode = now_ward),'0')'ward_bed','' as 'ward_standard' FROM ipd.ipd WHERE dateadm BETWEEN DATE_FORMAT(ADDDATE(NOW(),interval -1 year),'%Y-%m-%d') and DATE_FORMAT(NOW(),'%Y-%m-%d') and datedsc = '0000-00-00' and left(now_ward,1) != 'x' GROUP BY now_ward`);
    return data[0]; //Edit 2022-08-30
  }

  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
        SELECT CONCAT(s.regdate,'-',s.frequency,'-',s.seq)'seq',s.hn,s.an,p.pttitle 'title_name',p.ptfname 'first_name',p.ptlname 'last_name',REPLACE(refsendno,'/','-')'referno',s.daterefer 'referdate',hos.getRoomName(s.depart)'location_name',sendtohos 'to_hcode',c.ptclass 'pttype_id',hos.getPtClassName(c.ptclass)'pttype_name',SmartReferGetStrength(s.refhigh)'strength_id',hos.getHospitalName(s.sendtohos)'to_hcode_name',(SELECT d.cause FROM referdata d WHERE s.regdate = d.regdate and s.hn = d.hn and s.frequency = d.frequency and s.seq = d.seq limit 1)'refer_cause',s.timerefer 'refertime',getDoctorNo(s.doctor)'doctor',s.doctor as 'doctor_name' FROM sendrefer s LEFT JOIN pt.pt p on s.hn = p.hn LEFT JOIN pt.ptclass c on s.hn = c.hn WHERE s.daterefer BETWEEN '${start_date}' and '${end_date}' GROUP BY s.regdate,s.hn,s.frequency,s.seq`);

    return data[0];
  }

  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
        SELECT CONCAT(s.regdate,'-',s.frequency,'-',s.seq)'seq',s.hn,s.an,p.pttitle 'title_name',p.ptfname 'first_name',p.ptlname 'last_name',REPLACE(refsendno,'/','-')'referno',s.daterefer 'referdate',sendtohos 'to_hcode',hos.getHospitalName(s.sendtohos)'to_hcode_name',(SELECT d.cause FROM referdata d WHERE s.regdate = d.regdate and s.hn = d.hn and s.frequency = d.frequency and s.seq = d.seq limit 1)'refer_cause',s.timerefer 'refertime',getDoctorNo(s.doctor)'doctor',s.doctor as 'doctor_name' FROM sendrefer s LEFT JOIN pt.pt p on s.hn = p.hn LEFT JOIN pt.ptclass c on s.hn = c.hn WHERE s.daterefer BETWEEN '${start_date}' and '${end_date}' GROUP BY s.regdate,s.hn,s.frequency,s.seq`);

    return data[0];
  }

  async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
    let data = await db.raw(`
        SELECT CONCAT(s.regdate,'-',s.frequency,'-',s.seq)'seq',a.appointdate 'receive_appoint_date',a.timeappoint 'receive_appoint_beginttime',null as 'receive_apppoint_endtime',a.doctor 'receive_apppoint_doctor',hos.getClinicName(a.clinic)'receive_apppoint_chinic',null as 'receive_text',null as 'receive_by' FROM sendrefer s INNER JOIN pt.ptappoint a on s.regdate = a.regdate and a.hn = s.hn WHERE RIGHT(CONCAT('000000000',s.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) and a.appointdate = '${app_date}'`);

    return data[0];
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
            SELECT roomcode 'dep_code',roomname 'dep_name' FROM hos.roomno
        `);
    return data[0];

  }

  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
        SELECT hn FROM pt.pt WHERE REPLACE(cardid,'-','') = '${cid}'
        `);
    return data[0];
  }

  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT (
SELECT codehos FROM hosdata.confighos) AS 'drug_hospcode',(
SELECT nameHos FROM hosdata.confighos) AS 'drug_hospname',d.namedrug 'drug_name',d.item_usage 'drug_use',d.regdate 'drug_receive_date' FROM opd.drug_order_opd d INNER JOIN (
SELECT regdate,hn,frequency FROM opd.opd WHERE RIGHT(CONCAT('000000000',hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) ORDER BY regdate DESC LIMIT 2) AS o ON d.regdate=o.regdate AND o.hn=d.hn AND o.frequency=d.frequency`); //Edit 2022-08-30

    return data[0];
  }

  static DateToYYYYMMDD(Date: Date): String {
    let DS: string = Date.getFullYear()
      + '-' + ('0' + (Date.getMonth() + 1)).slice(-2)
      + '-' + ('0' + Date.getDate()).slice(-2);
    return DS;
  }

  async getServicesCoc(db: Knex, hn: any) {
    let cmd = `SELECT o.hn 'hn', CONCAT(o.regdate,'-',o.frequency,'-',o.docno)'seq',
    p.pttitle 'title_name',p.ptfname 'first_name',p.ptlname 'last_name',
    o.regdate 'date_serv',o.timereg 'time_serv',hos.getRoomName(o.sendScrRoom)'department' 
    FROM opd.opd o 
    INNER JOIN pt.pt p on o.hn = p.hn 
    WHERE RIGHT(CONCAT('000000000',p.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) ORDER BY seq DESC limit 3`;
    let data = await db.raw(cmd);
    return data[0];
  }

  async getProfileCoc(db: Knex, hn: any) {
    let cmd = `SELECT p.hn,REPLACE(cardid,'-','')'cid',pttitle 'title_name',ptfname 'first_name',ptlname 'last_name',if(ptsex='sx1','ชาย','หญิง')'sex',ptvillage 'moopart',ptaddress 'addrpart',pttambon 'tmbpart',ptamphur 'amppart',ptprovince 'chwpart',ptdob 'brthdate',concat(right(concat('000',timestampdiff(YEAR,ptdob,NOW())),3) ,'-',right(concat('00',timestampdiff(month,ptdob,NOW())-(timestampdiff(year,ptdob,NOW())*12)),2), '-',right(concat('00',timestampdiff(day,date_add(ptdob,interval (timestampdiff(month,ptdob,NOW())) month),curdate())),2)) 'age',c.ptclass 'pttype_id',(SELECT i.NAME FROM hos.insclasses i WHERE c.ptclass = i.code)'pttype_name',c.classNo 'pttype_no',c.mainhos 'hospmain',hos.getHospitalName(c.mainhos)'hospmain_name',c.subhos 'hospsub',hos.getHospitalName(c.subhos)'hospsub_name',p.regdate 'registdate',p.regdate 'visitdate',CONCAT(p.fattitle,p.fatfname,' ',p.fatlname)'father_name',CONCAT(p.mottitle,p.motfname,' ',p.motlname)'mother_name','' as 'couple_name',CONCAT(p.contacttitle,p.contactfname,' ',p.contactlname)'contact_name',(SELECT h.NAME FROM hos.codeinhos h WHERE h.code = p.relation)'contact_relation',p.ptphone 'contact_mobile' FROM pt.pt p LEFT JOIN pt.ptclass c on p.hn = c.hn WHERE RIGHT(CONCAT('000000000',p.hn),9) = RIGHT(CONCAT('000000000','${hn}'),9) GROUP BY p.hn `;
    let data = await db.raw(cmd);
    return data[0];
  }
}
