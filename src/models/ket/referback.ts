import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class RerferBackModel {
    async rfback_select_cid(token:any, cid: any,hcode: any,refer: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/selectCid/${cid}/${hcode}/${refer}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfback_select(token:any, hcode: any, sdate: any, edate: any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/select/${hcode}/${sdate}/${edate}/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfback_select_reply(token:any, hcode: any, sdate: any, edate: any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/select_reply/${hcode}/${sdate}/${edate}/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfback_selectOne(token:any, refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/selectOne/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfbackInsert(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'POST',
                url: `${urlApi}/referback/insert`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows }
                ,
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfbackUpdate(token:any, refer_no: any, rows: any) {
        // console.log(token);
        
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'PUT',
                url: `${urlApi}/referback/update/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows }
                ,
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfbackReceive(token:any, refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/receive/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }


    async count_referback(token:any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/count_referback/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_referback_reply(token:any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/count_referback_reply/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_report(token:any, stdate: any, endate: any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/count_report/${stdate}/${endate}/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_report_reply(token:any, stdate: any, endate: any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/count_report_reply/${stdate}/${endate}/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async del_referback(token:any, refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/delete/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async del_user_referback(token:any, refer_no: any, providerUser: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/delete_refer/${refer_no}/${providerUser}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfback_selectCID(token:any, cid: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referback/selectCID/${cid}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
}