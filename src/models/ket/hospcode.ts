import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_COC_API_URL;

export class HospcodeModel {

    async select(token:any, hospcode:string){
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/hospcode/select/${hospcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async selectAll(token:any){
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/hospcode/select_all`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async selectByProvcode(token:any, prov_code:string){
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/hospcode/select_by_provcode/${prov_code}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async selectKet(token:any, hospcode:string){
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/hospcode/select_ket/${hospcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async selectSameDistrict(token:any, hospcode:string){
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/hospcode/select_same_district/${hospcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
}