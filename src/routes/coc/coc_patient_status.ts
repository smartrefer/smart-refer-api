import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { CocPatientStatusModel } from '../../models/coc/coc_patient_status'

const fromImportModel = new CocPatientStatusModel();
export default async function CocPatientStatus(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await fromImportModel.select(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
        })


    
}