import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { CocEvaluateModel } from '../../models/coc/coc_evaluate'

const fromImportModel = new CocEvaluateModel();
export default async function CocEvaluate(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await fromImportModel.select(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
        })



    // insert
    fastify.post('/insert',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body;
        console.log(info);
        try {
          let res_: any = await fromImportModel.insert(token, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //update?evauate_id=xxx
    fastify.put('/update',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let evauate_id = req.query.evauate_id
        let info = req.body
        try {
          let res_: any = await fromImportModel.update(token, evauate_id, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
      
    })

    //delete?evauate_id=xxx
    fastify.get('/delete',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let evauate_id = req.query.evauate_id
        try {
          let res_: any = await fromImportModel.delete(token, evauate_id);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.post('/selectOne',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
      const req: any = request
      const token = req.headers.authorization.split(' ')[1];

      let info = req.body;
      console.log(info);
      try {
        let res_: any = await fromImportModel.selectOne(token, info);
        reply.send(res_);
      } catch (error) {
          reply.send({ ok: false, error: error });
      }
  })

  fastify.post('/select_evaluate',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request
    const token = req.headers.authorization.split(' ')[1];

    let info = req.body;
    console.log(info);
    try {
      let res_: any = await fromImportModel.select_evaluate(token, info);
      reply.send(res_);
    } catch (error) {
        reply.send({ ok: false, error: error });
    }
})
    
}