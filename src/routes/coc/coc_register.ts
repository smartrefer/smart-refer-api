import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { CocRegisterModel } from '../../models/coc/coc_register'

const fromImportModel = new CocRegisterModel();
export default async function CocRegister(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await fromImportModel.select(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
        })


    // insert
    fastify.post('/insert',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body
        // console.log(info);
        try {
          let res_: any = await fromImportModel.insert(token, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //update?coc_register_id=xxx
    fastify.put('/update',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let coc_register_id = req.query.coc_register_id
        let info = req.body
        try {
          let res_: any = await fromImportModel.update(token, coc_register_id, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
      
    })

    //delete?coc_register_id=xxx
    fastify.get('/delete',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let coc_register_id = req.query.coc_register_id
        try {
          let res_: any = await fromImportModel.delete(token, coc_register_id);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.post('/selectOne',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
      const req: any = request
      const token = req.headers.authorization.split(' ')[1];

      let info = req.body;
      console.log(info);
      try {
        let res_: any = await fromImportModel.selectOne(token, info);
        reply.send(res_);
      } catch (error) {
          reply.send({ ok: false, error: error });
      }
  })    

    fastify.post('/selectHcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
      const req: any = request
      const token = req.headers.authorization.split(' ')[1];

      let info = req.body
      // console.log(info);
      try {
        let res_: any = await fromImportModel.selectHcode(token, info);
        reply.send(res_);
      } catch (error) {
          reply.send({ ok: false, error: error });
      }
  })

  fastify.post('/selectFromHcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request
    const token = req.headers.authorization.split(' ')[1];

    let info = req.body
    // console.log(info);
    try {
      let res_: any = await fromImportModel.selectFromHcode(token, info);
      reply.send(res_);
    } catch (error) {
        reply.send({ ok: false, error: error });
    }
})

  fastify.post('/selectCid',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request
    const token = req.headers.authorization.split(' ')[1];

    let info = req.body
    // console.log(info);
    try {
      let res_: any = await fromImportModel.selectCid(token, info);
      reply.send(res_);
    } catch (error) {
        reply.send({ ok: false, error: error });
    }
})

}