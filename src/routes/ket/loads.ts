import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { LoadsModel } from '../../models/ket/loads'

const loadsModel = new LoadsModel();
export default async function loads(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        try {
            let res_: any = await loadsModel.loads(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }   
    })


}