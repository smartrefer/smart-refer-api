import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { HolidayModel } from '../../models/ket/holiday'

const holidayModel = new HolidayModel();
export default async function barthelRate(fastify: FastifyInstance) {

    // select
    fastify.get('/list', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await holidayModel.list(token);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let id = req.query.id
        try {
            let res_: any = await holidayModel.select_id(token, id);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // insert
    fastify.post('/insert', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body.rows;
        // console.log(info);
        try {
            let res_: any = await holidayModel.insert(token, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/update', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let id = req.query.id
        let info = req.body
        try {
            let res_: any = await holidayModel.update(token, id, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }

    })

    fastify.get('/delete', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let id = req.query.id
        try {
            let res_: any = await holidayModel.delete(token, id);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })



}