import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { HospcodeModel } from '../../models/ket/hospcode'

const hospcodeModel = new HospcodeModel();
export default async function evaluate(fastify: FastifyInstance) {

    fastify.get('/select/:hospcode',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req :any = request;
        const hospcode = req.params.hospcode;
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await hospcodeModel.select(token,hospcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    });

    fastify.get('/select_all',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req :any = request;
        const token = req.headers.authorization.split(' ')[1];
        try {
            let res_: any = await hospcodeModel.selectAll(token);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    });

    fastify.get('/select_by_provcode/:prov_code',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req :any = request;
        const prov_code = req.params.prov_code;
        const token = req.headers.authorization.split(' ')[1];
        try {
            let res_: any = await hospcodeModel.selectByProvcode(token,prov_code);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    });

    fastify.get('/select_ket/:hospcode',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req :any = request;
        const hospcode = req.params.hospcode;
        const token = req.headers.authorization.split(' ')[1];
        try {
            let res_: any = await hospcodeModel.selectKet(token,hospcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    });

    fastify.get('/select_same_district/:hospcode',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req :any = request;
        const hospcode = req.params.hospcode;
        const token = req.headers.authorization.split(' ')[1];
        try {
            let res_: any = await hospcodeModel.selectSameDistrict(token,hospcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    });

}
