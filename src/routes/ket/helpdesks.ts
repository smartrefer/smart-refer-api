import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { HelpdesksModel } from '../../models/ket/helpdesks'

const helpdesksModel = new HelpdesksModel();
export default async function referResult(fastify: FastifyInstance) {
const db = fastify.mysql2;
  
fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
  reply.send({ ok: true, message: 'Welcome to SmartRefer Clinent!'});
});


fastify.get('/select', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;

  try {
    let res_: any = await helpdesksModel.select(db);
    reply.send(res_);
  } catch (error) {
    reply.send({ ok: false, error: error });
  }
});


fastify.post('/insert', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
  
    let info = req.body
    try {
      let res_: any = await helpdesksModel.insert(db, info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
  });
  
  //update?id=xxx
  fastify.put('/update', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
  
    let id = req.query.id
    let info = req.body
    try {
      let res_: any = await helpdesksModel.update(db, id, info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
  });
  
  //delete?id=xxx
  fastify.delete('/delete', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
  
    let id = req.query.id
    try {
      let res_: any = await helpdesksModel.delete(db, id);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
  });
  
}