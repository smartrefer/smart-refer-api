import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ReferLogModel } from '../../models/ket/refer_log'

const referLogModel = new ReferLogModel();
export default async function referLog(fastify: FastifyInstance) {

    // select
    fastify.get('/select/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
          let res_: any = await referLogModel.referLog(token, refer_no);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.post('/inser',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body
        try {
          let res_: any = await referLogModel.insert(token, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })
}